package com.dstvdm.gol2.tests;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

/**
 * Created by Joshua.Lewis on 2014/06/27.
 */
public class Tests {

    @Test
    public void Test_Barge() {
        char[][] initialGrid = new char[][]
                {
                        {'.', '.', '.', '.', '.', '.'},
                        {'.', '.', '*', '.', '.', '.'},
                        {'.', '*', '.', '*', '.', '.'},
                        {'.', '.', '*', '.', '*', '.'},
                        {'.', '.', '.', '*', '.', '.'},
                        {'.', '.', '.', '.', '.', '.'},
                };

        Game game = new Game(initialGrid);

        game.Tick();
        char[][] generation = game.getGrid();

        assertArrayEquals(generation, initialGrid);

    }

    @Test
    public void Test_Blinker()
    {
        char[][] initialGrid = new char[][]
        {
            {'.', '.', '.'},
            {'*', '*', '*'},
            {'.', '.', '.'},
        };

        char[][] expectedGeneration1 = new char[][]
        {
            {'.', '*', '.'},
            {'.', '*', '.'},
            {'.', '*', '.'},
        };

        char[][] expectedGeneration2 = new char[][]
        {
            {'.', '.', '.'},
            {'*', '*', '*'},
            {'.', '.', '.'},
        };

        Game game = new Game(initialGrid);

        game.Tick();
        char[][] actualGeneration = game.getGrid();

        assertArrayEquals(actualGeneration, expectedGeneration1);

        game.Tick();
        actualGeneration = game.getGrid();

        assertArrayEquals(actualGeneration, expectedGeneration2);

    }

    @Test
    public void Test_Glider()
    {
        char[][] initialGrid = new char[][]
        {
            {'.', '*', '.'},
            {'.', '.', '*'},
            {'*', '*', '*'},
        };

        char[][] expectedGeneration1 = new char[][]
        {
            {'.', '.', '.'},
            {'*', '.', '*'},
            {'.', '*', '*'},
            {'.', '*', '.'},
        };

        char[][] expectedGeneration2 = new char[][]
        {
            {'.', '.', '.'},
            {'.', '.', '*'},
            {'*', '.', '*'},
            {'.', '*', '*'},
        };

        Game game = new Game(initialGrid);

        char[][] actualGeneration = game.getGrid();
        assertArrayEquals(actualGeneration, expectedGeneration1);

        game.Tick();
        actualGeneration = game.getGrid();

        game.Tick();
        assertArrayEquals(actualGeneration, expectedGeneration2);


    }

    @Test
    public void Test_Glider_090()
    {
        char[][] initialGrid = new char[][]
        {
            {'*', '.', '.'},
            {'*', '.', '*'},
            {'*', '*', '.'},
        };

        char[][] expectedGeneration1 = new char[][]
        {
            {'.', '.', '*', '.'},
            {'*', '*', '.', '.'},
            {'.', '*', '*', '.'},

        };

        char[][] expectedGeneration2 = new char[][]
        {
            {'.', '*', '.', '.'},
            {'*', '.', '.', '.'},
            {'*', '*', '*', '.'},
        };

        Game game = new Game(initialGrid);
        Game.PrintGrid(initialGrid);

        game.Tick();
        char[][] actualGeneration = game.getGrid();
        assertArrayEquals(actualGeneration, expectedGeneration1);

        game.Tick();
        actualGeneration = game.getGrid();
        assertArrayEquals(actualGeneration, expectedGeneration2);

    }

    @Test
    public void Test_Glider_180()
    {
        char[][] initialGrid = new char[][]
        {
            {'*', '*', '*'},
            {'*', '.', '.'},
            {'.', '*', '.'},
        };

        char[][] expectedGeneration1 = new char[][]
        {
            {'.', '*', '.'},
            {'*', '*', '.'},
            {'*', '.', '*'},
            {'.', '.', '.'},
        };

        char[][] expectedGeneration2 = new char[][]
        {
            {'*', '*', '.'},
            {'*', '.', '*'},
            {'*', '.', '.'},
            {'.', '.', '.'},
        };

        Game game = new Game(initialGrid);
        Game.PrintGrid(initialGrid);

        game.Tick();
        char[][] actualGeneration = game.getGrid();
        assertArrayEquals(actualGeneration, expectedGeneration1);

        game.Tick();
        actualGeneration = game.getGrid();
        assertArrayEquals(actualGeneration, expectedGeneration2);

    }

    @Test
    public void Test_Glider_270()
    {
        char[][] initialGrid = new char[][]
        {
            {'.', '*', '*'},
            {'*', '.', '*'},
            {'.', '.', '*'},
        };

        char[][] expectedGeneration1 = new char[][]
        {
            {'.', '*', '*', '.'},
            {'.', '.', '*', '*'},
            {'.', '*', '.', '.'},

        };

        char[][] expectedGeneration2 = new char[][]
        {
            {'.', '*', '*', '*'},
            {'.', '.', '.', '*'},
            {'.', '.', '*', '.'},
        };

        Game game = new Game(initialGrid);
        Game.PrintGrid(initialGrid);

        game.Tick();
        char[][] actualGeneration = game.getGrid();
        assertArrayEquals(actualGeneration, expectedGeneration1);

        game.Tick();
        actualGeneration = game.getGrid();
        assertArrayEquals(actualGeneration, expectedGeneration2);

    }

}
