using System;
using System.Collections.Generic;
using System.Linq;

namespace Tests
{
    public class DirtyGame : Game
    {
        private char[,] grid;

        public DirtyGame(char[,] grid)
        {
            this.grid = grid;
        }

        public override char[,] Grid
        {
            get { return grid; }
        }

        public override void Tick()
        {
            var newLiveCellCoords = CalculateNewLiveCellCoords();

            var newGrid = GenerateNewGrid(newLiveCellCoords);

            PrintGrid(newGrid);
            grid = newGrid;
        }

        private char[,] GenerateNewGrid(List<Tuple<int, int>> newLiveCellCoords)
        {
            char[,] newGrid = GenerateEmptyGrid(newLiveCellCoords);
            newLiveCellCoords = AdjustForNegativeCoords(newLiveCellCoords);

            foreach (var coord in newLiveCellCoords)
            {
                newGrid[coord.Item1, coord.Item2] = '*';
            }
            return newGrid;
        }

        private List<Tuple<int, int>> CalculateNewLiveCellCoords()
        {
            var cellMap = GenerateCellMapwithFramingCells();

            return CalculateNewLiveCellCoords(cellMap);
        }

        private List<Tuple<int, int>> CalculateNewLiveCellCoords(Dictionary<Tuple<int, int>, char> currentCellMap)
        {
            var currentLiveCellCoords = currentCellMap
                .Where(x => x.Value == '*')
                .Select(x => new Tuple<int, int>(x.Key.Item1, x.Key.Item2))
                ;
            var newLiveCellCoords = new List<Tuple<int, int>>();

            foreach (var coord in currentCellMap.Keys)
            {
                var numberOfLiveNeighbours = CalculateNumberOfLiveNeighbours(coord, currentLiveCellCoords);

                if (currentLiveCellCoords.Contains(coord)) //cell is currently alive
                {
                    if (numberOfLiveNeighbours == 2 || numberOfLiveNeighbours == 3)
                    {
                        newLiveCellCoords.Add(coord);
                    }
                }
                else //cell is dead
                {
                    if (numberOfLiveNeighbours == 3)
                    {
                        newLiveCellCoords.Add(coord);
                    }
                }
            }
            return newLiveCellCoords;
        }

        private Dictionary<Tuple<int, int>, char> GenerateCellMapwithFramingCells()
        {
            var cellMap = GenerateCellMapForGrid();

            foreach (var frameCoord in  GetFramingCoords())
            {
                cellMap.Add(new Tuple<int, int>(frameCoord.Item1, frameCoord.Item2), '.');
            }
            return cellMap;
        }

        private Dictionary<Tuple<int, int>, char> GenerateCellMapForGrid()
        {
            var cellMap = new Dictionary<Tuple<int, int>, char>();
            for (int i = 0; i < grid.GetLength(0); i++)
            {
                for (int j = 0; j < grid.GetLength(1); j++)
                {
                    cellMap.Add(new Tuple<int, int>(i, j), grid[i, j]);
                }
            }
            return cellMap;
        }

        private List<Tuple<int, int>> AdjustForNegativeCoords(List<Tuple<int, int>> newLiveCellCoords)
        {
            if (newLiveCellCoords.Any(x => x.Item1 < 0)) //moved 'up'
            {
                newLiveCellCoords = newLiveCellCoords
                    .Select(x => new Tuple<int, int>(x.Item1 + 1, x.Item2))
                    .ToList()
                    ;
            }

            if (newLiveCellCoords.Any(x => x.Item2 < 0)) //moved 'left'
            {
                newLiveCellCoords = newLiveCellCoords
                    .Select(x => new Tuple<int, int>(x.Item1, x.Item2 + 1))
                    .ToList()
                    ;
            }
            return newLiveCellCoords;
        }

        private char[,] GenerateEmptyGrid(IEnumerable<Tuple<int, int>> newLiveCellCoords)
        {
            Tuple<int, int> cornerCoord = GetSizeOfNewGrid(newLiveCellCoords);

            var newGrid = new char[cornerCoord.Item1, cornerCoord.Item2];

            for (int i = 0; i < cornerCoord.Item1; i++)
            {
                for (int j = 0; j < cornerCoord.Item2; j++)
                {
                    newGrid[i, j] = '.';
                }
            }

            return newGrid;
        }

        private Tuple<int, int> GetSizeOfNewGrid(IEnumerable<Tuple<int, int>> newLiveCellCoords)
        {
            var maxX = newLiveCellCoords
                .Max(x => x.Item1) + 1
                ;

            if (maxX < grid.GetLength(0))
                maxX = grid.GetLength(0);

            var maxY = newLiveCellCoords
                .Max(x => x.Item2) + 1
                ;
            if (maxY < grid.GetLength(1))
                maxY = grid.GetLength(1);

            if (newLiveCellCoords.Any(x => x.Item1 < 0))//moved 'upwards'
            {
                maxX++;
            }

            if (newLiveCellCoords.Any(x => x.Item2 < 0))//moved 'left'
            {
                maxY++;
            }

            return new Tuple<int, int>(maxX, maxY);
        }

        private List<Tuple<int, int>> GetFramingCoords()
        {
            var framingCoords = new List<Tuple<int, int>>();
            for (int i = -1; i <= grid.GetLength(0); i++)
            {
                for (int j = -1; j <= grid.GetLength(1); j++)
                {
                    framingCoords.Add(new Tuple<int, int>(i, j));
                }
            }

            var existingCoords = new List<Tuple<int, int>>();
            for (int i = 0; i < grid.GetLength(0); i++)
            {
                for (int j = 0; j < grid.GetLength(1); j++)
                {
                    existingCoords.Add(new Tuple<int, int>(i, j));
                }
            }

            return framingCoords.Except(existingCoords).ToList();
        }

        private int CalculateNumberOfLiveNeighbours(Tuple<int, int> coord, IEnumerable<Tuple<int, int>> liveCellCoords)
        {
            var i = coord.Item1;
            var j = coord.Item2;
            var neighbourCoordinates = new List<Tuple<int, int>>()
            {
                new Tuple<int, int>(i - 1, j - 1),
                new Tuple<int, int>(i, j - 1),
                new Tuple<int, int>(i + 1, j - 1),
                new Tuple<int, int>(i - 1, j),
                new Tuple<int, int>(i + 1, j),
                new Tuple<int, int>(i - 1, j + 1),
                new Tuple<int, int>(i, j + 1),
                new Tuple<int, int>(i + 1, j + 1),
            };

            var numberOfLiveNeighbours =
                neighbourCoordinates
                    .Where(
                        coordinate =>
                            coordinate.Item1 > -1 &&
                            coordinate.Item2 > -1 &&
                            coordinate.Item1 < grid.GetLength(0) &&
                            coordinate.Item2 < grid.GetLength(1))
                    .Count(
                        coordinate => liveCellCoords.Contains(new Tuple<int, int>(coordinate.Item1, coordinate.Item2)));
            return numberOfLiveNeighbours;
        }

    }
}