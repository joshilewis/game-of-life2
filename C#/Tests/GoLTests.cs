﻿using System;
using System.Text;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class GoLTests
    {
        [Test]
        public void Test_Barge()
        {
            var initialGrid = new char[,]
            {
                {'.', '.', '.', '.', '.', '.'},
                {'.', '.', '*', '.', '.', '.'},
                {'.', '*', '.', '*', '.', '.'},
                {'.', '.', '*', '.', '*', '.'},
                {'.', '.', '.', '*', '.', '.'},
                {'.', '.', '.', '.', '.', '.'},
            };

            Game.PrintGrid(initialGrid);

            var game = CreateGame(initialGrid);
            game.Tick();
            char[,] generation = game.Grid;

            Assert.That(generation, Is.EqualTo(initialGrid));

        }

        [Test]
        public void Test_Blinker()
        {
            var initialGrid = new char[,]
            {
                {'.', '.', '.'},
                {'*', '*', '*'},
                {'.', '.', '.'},
            };

            var expectedGeneration1 = new char[,]
            {
                {'.', '*', '.'},
                {'.', '*', '.'},
                {'.', '*', '.'},
            };

            var expectedGeneration2 = new char[,]
            {
                {'.', '.', '.'},
                {'*', '*', '*'},
                {'.', '.', '.'},
            };

            var game = CreateGame(initialGrid);
            Game.PrintGrid(initialGrid);
            game.Tick();

            char[,] actualGeneration = game.Grid;

            Assert.That(actualGeneration, Is.EqualTo(expectedGeneration1));

            game.Tick();
            actualGeneration = game.Grid;

            Assert.That(actualGeneration, Is.EqualTo(expectedGeneration2));

        }

        [Test]
        public void Test_Glider()
        {
            var initialGrid = new char[,]
            {
                {'.', '*', '.'},
                {'.', '.', '*'},
                {'*', '*', '*'},
            };

            var expectedGeneration1 = new char[,]
            {
                {'.', '.', '.'},
                {'*', '.', '*'},
                {'.', '*', '*'},
                {'.', '*', '.'},
            };

            var expectedGeneration2 = new char[,]
            {
                {'.', '.', '.'},
                {'.', '.', '*'},
                {'*', '.', '*'},
                {'.', '*', '*'},
            };

            var game = CreateGame(initialGrid);
            Game.PrintGrid(initialGrid);

            game.Tick();
            char[,] actualGeneration = game.Grid;

            Assert.That(actualGeneration, Is.EqualTo(expectedGeneration1));

            game.Tick();
            actualGeneration = game.Grid;

            Assert.That(actualGeneration, Is.EqualTo(expectedGeneration2));

            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();

        }

        [Test]
        public void Test_Glider_090()
        {
            var initialGrid = new char[,]
            {
                {'*', '.', '.'},
                {'*', '.', '*'},
                {'*', '*', '.'},
            };

            var expectedGeneration1 = new char[,]
            {
                {'.', '.', '*', '.'},
                {'*', '*', '.', '.'},
                {'.', '*', '*', '.'},

            };

            var expectedGeneration2 = new char[,]
            {
                {'.', '*', '.', '.'},
                {'*', '.', '.', '.'},
                {'*', '*', '*', '.'},
            };

            var game = CreateGame(initialGrid);
            Game.PrintGrid(initialGrid);

            game.Tick();
            char[,] actualGeneration = game.Grid;

            Assert.That(actualGeneration, Is.EqualTo(expectedGeneration1));

            game.Tick();
            actualGeneration = game.Grid;

            Assert.That(actualGeneration, Is.EqualTo(expectedGeneration2));
            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();
        }

        [Test]
        public void Test_Glider_180()
        {
            var initialGrid = new char[,]
            {
                {'*', '*', '*'},
                {'*', '.', '.'},
                {'.', '*', '.'},
            };

            var expectedGeneration1 = new char[,]
            {
                {'.', '*', '.'},
                {'*', '*', '.'},
                {'*', '.', '*'},
                {'.', '.', '.'},
            };

            var expectedGeneration2 = new char[,]
            {
                {'*', '*', '.'},
                {'*', '.', '*'},
                {'*', '.', '.'},
                {'.', '.', '.'},
            };

            var game = CreateGame(initialGrid);
            Game.PrintGrid(initialGrid);

            game.Tick();
            char[,] actualGeneration = game.Grid;

            Assert.That(actualGeneration, Is.EqualTo(expectedGeneration1));

            game.Tick();
            actualGeneration = game.Grid;

            Assert.That(actualGeneration, Is.EqualTo(expectedGeneration2));
            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();
        }

        [Test]
        public void Test_Glider_270()
        {
            var initialGrid = new char[,]
            {
                {'.', '*', '*'},
                {'*', '.', '*'},
                {'.', '.', '*'},
            };

            var expectedGeneration1 = new char[,]
            {
                {'.', '*', '*', '.'},
                {'.', '.', '*', '*'},
                {'.', '*', '.', '.'},

            };

            var expectedGeneration2 = new char[,]
            {
                {'.', '*', '*', '*'},
                {'.', '.', '.', '*'},
                {'.', '.', '*', '.'},
            };

            var game = CreateGame(initialGrid);
            Game.PrintGrid(initialGrid);

            game.Tick();
            char[,] actualGeneration = game.Grid;
            Assert.That(actualGeneration, Is.EqualTo(expectedGeneration1));

            game.Tick();
            actualGeneration = game.Grid;
            Assert.That(actualGeneration, Is.EqualTo(expectedGeneration2));

            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();
            game.Tick();
        }

        private Game CreateGame(char[,] initialGrid)
        {
            return new DirtyGame(initialGrid);
        }
    }
}
